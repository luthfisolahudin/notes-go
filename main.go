package main

import (
	"errors"
	"io"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"
	"time"

	"dario.cat/mergo"
	"github.com/BurntSushi/toml"
	"github.com/urfave/cli/v2"
)

type (
	Config struct {
		Categories map[string]Category `toml:"categories"`
	}

	Category struct {
		Editor         string `toml:"editor"`
		Stubs          string `toml:"stubs"`
		Path           string `toml:"path"`
		Filename       string `toml:"filename"`
		TitleSeparator string `toml:"title_separator"`
		Extension      string `toml:"ext"`
	}
)

func (c Config) ResolveCategory(name string) (Category, error) {
	category, found := c.Categories[name]

	if !found {
		return Category{}, errors.New("category not found")
	}

	defaultCategory, found := c.Categories["default"]

	if !found {
		return Category{}, errors.New("default category not found")
	}

	mergo.Merge(&category, defaultCategory)

	return category, nil
}

func logInfo(silent bool, info string) {
	if !silent {
		log.Println(info)
	}
}

func logError(silent bool, err error) error {
	if silent {
		return nil
	}

	log.Fatalln(err)

	return err
}

func cmd(command string) ([]byte, error) {
	var output []byte
	var err error

	if runtime.GOOS == "windows" {
		output, err = exec.Command("cmd", "/C", command).Output()
	} else {
		output, err = exec.Command("sh", "-c", command).Output()
	}

	if err != nil {
		return nil, err
	}

	return output, nil
}

// See https://stackoverflow.com/a/21067803
func copyFile(src string, dest string) (err error) {
	in, err := os.Open(src)

	if err != nil {
		return
	}

	defer in.Close()

	out, err := os.Create(dest)

	if err != nil {
		return
	}

	defer func() {
		cerr := out.Close()

		if err == nil {
			err = cerr
		}
	}()

	if _, err = io.Copy(out, in); err != nil {
		return
	}

	err = out.Sync()

	return
}

func main() {
	var configPath string
	var config Config
	var silent bool

	app := &cli.App{
		Name:  "notes",
		Usage: "Personal note helper",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "config",
				Aliases:     []string{"C"},
				Value:       "./config.toml",
				Destination: &configPath,
			},
			&cli.BoolFlag{
				Name:        "silent",
				Aliases:     []string{"s"},
				Value:       false,
				Destination: &silent,
			},
		},
		Before: func(ctx *cli.Context) error {
			if _, err := toml.DecodeFile(configPath, &config); err != nil {
				return logError(silent, errors.New("failed decode config: "+err.Error()))
			}

			return nil
		},
		Commands: []*cli.Command{
			{
				Name:    "new",
				Aliases: []string{"n"},
				Usage:   "Create new note",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:    "category",
						Aliases: []string{"c"},
						Value:   "default",
					},
					&cli.BoolFlag{
						Name:    "open-editor",
						Aliases: []string{"e"},
					},
				},
				Action: func(ctx *cli.Context) error {
					var category Category
					var err error

					if category, err = config.ResolveCategory(ctx.String("category")); err != nil {
						return logError(silent, errors.New("category not found: "+err.Error()))
					}

					suffix := ""

					if ctx.Args().Present() {
						suffix += category.TitleSeparator + strings.Join(ctx.Args().Slice(), " ")
					}

					date := time.Now().Local()
					filename := date.Format(category.Filename) + suffix
					basename := filename + category.Extension

					path := filepath.Join(category.Path, basename)
					folder := filepath.Dir(path)

					if _, err := os.Stat(path); !errors.Is(err, os.ErrNotExist) {
						return logError(silent, errors.New("note already exist"))
					}

					if err := os.MkdirAll(folder, os.ModePerm); err != nil {
						return logError(silent, err)
					}

					if category.Stubs == "" {
						if _, err := os.Create(path); err != nil {
							return logError(silent, err)
						}
					} else if err := copyFile(category.Stubs, path); err != nil {
						return logError(silent, err)
					}

					logInfo(silent, "note "+path+" created")

					if ctx.Bool("open-editor") {
						command := category.Editor

						if strings.Contains(command, "{}") {
							command = strings.Replace(command, "{}", path, -1)
						} else {
							command += " " + path
						}

						if _, err := cmd(command); err != nil {
							logError(silent, err)
						}
					}

					return nil
				},
			},
		},
	}

	if err := app.Run(os.Args); err != nil {
		logError(silent, err)
	}
}
