# Notes — written in [Go](https://go.dev)

A personal note helper for creating note in various directories with support for each own template, file naming, and extension.

## Usage

```
$ notes --help
NAME:
   notes - Personal note helper

USAGE:
   notes [global options] command [command options]

COMMANDS:
   new, n   Generate new note
   help, h  Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --config value, -C value  (default: "./config.toml")
   --silent, -s              (default: false)
   --help, -h                show help
```

```
$ notes new --help
NAME:
   notes new - Create new note

USAGE:
   notes new [command options] [arguments...]

OPTIONS:
   --category value, -c value  (default: "default")
   --open-editor, -e           (default: false)
   --help, -h                  show help
```
