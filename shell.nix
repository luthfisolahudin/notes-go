{ sources ? import ./nix/sources.nix }:

let
  go_version = "1.22";

  pkgs = import sources.nixpkgs { overlays = []; config = {}; };

  go = pkgs."go_${builtins.replaceStrings ["."] ["_"] go_version}";
  niv = (import sources.niv {}).niv;
in

pkgs.mkShell {
  buildInputs = [
    go
    niv
  ];
}
